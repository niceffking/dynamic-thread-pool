package cn.niceff.middleware.dynamic.thread.pool.sdk.domain;

import cn.niceff.middleware.dynamic.thread.pool.sdk.domain.model.entity.ThreadPoolConfigEntity;

import java.util.List;

/**
 * @author niceff.cn
 * 动态线程池的服务接口
 */
public interface IPoolService {
    /**
     * 查询线程池列表
     *
     * @return
     */
    List<ThreadPoolConfigEntity> queryThreadPoolList();

    /**
     * 根据线程池名称查询线程池配置
     *
     * @param threadPoolName 线程池名称
     * @return 线程池配置
     */
    ThreadPoolConfigEntity queryThreadPoolConfigByName(String threadPoolName);

    /**
     * 更新线程池配置
     *
     * @param threadPoolConfigEntity 线程池配置
     */
    void updateThreadPoolConfig(ThreadPoolConfigEntity threadPoolConfigEntity);
}
