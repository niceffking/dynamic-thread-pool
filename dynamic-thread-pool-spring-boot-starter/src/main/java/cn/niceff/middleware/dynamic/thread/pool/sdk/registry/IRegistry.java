package cn.niceff.middleware.dynamic.thread.pool.sdk.registry;

import cn.niceff.middleware.dynamic.thread.pool.sdk.domain.model.entity.ThreadPoolConfigEntity;

import java.util.List;

/**
 * @author niceff.cn
 * @apiNote 统一注册中心接口
 */
public interface IRegistry {

    /**
     * 上报线程池List
     * @param threadPoolConfigEntities 线程池配置列表
     */
    void reportThreadPool(List<ThreadPoolConfigEntity> threadPoolConfigEntities);

    /**
     * 上报线程池配置参数
     * @param threadPoolConfigEntity 线程池配置实体
     */
    void reportThreadPoolConfigParameter(ThreadPoolConfigEntity threadPoolConfigEntity);
}
