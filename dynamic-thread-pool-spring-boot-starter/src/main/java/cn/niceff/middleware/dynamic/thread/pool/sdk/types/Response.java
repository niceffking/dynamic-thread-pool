package cn.niceff.middleware.dynamic.thread.pool.sdk.types;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author niceff.cn
 * 统一返回值格式
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Response<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    private int code;
    private String message;
    private T data;

    public enum CodeEnum {
        /** 信息枚举*/
        SUCCESS(200, "成功"),
        FAIL(500, "失败"),
        ILLEGAL_ARGUMENT(400, "参数不合法"),
        ;

        private int code;
        private String message;

        CodeEnum(int code, String message) {
            this.code = code;
            this.message = message;
        }

        public int getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }
    }

}
