package cn.niceff.middleware.dynamic.thread.pool.sdk.registry.redis;

import cn.niceff.middleware.dynamic.thread.pool.sdk.domain.model.entity.ThreadPoolConfigEntity;
import cn.niceff.middleware.dynamic.thread.pool.sdk.domain.model.valobj.RegistryEnumVO;
import cn.niceff.middleware.dynamic.thread.pool.sdk.registry.IRegistry;
import org.redisson.api.RBucket;
import org.redisson.api.RList;
import org.redisson.api.RedissonClient;

import java.time.Duration;
import java.util.List;

/**
 * @author niceff.cn
 * @apiNote 基于redis的注册中心服务, 提供了很多便捷的注册反复噶
 */
public class RedisRegistry implements IRegistry {

    private RedissonClient redisClient;

    public RedisRegistry(RedissonClient redisClient) {
        this.redisClient = redisClient;
    }

    @Override
    public void reportThreadPool(List<ThreadPoolConfigEntity> threadPoolConfigEntities) {
        RList<ThreadPoolConfigEntity> list = redisClient.getList(RegistryEnumVO.THREAD_POOL_CONFIG_LIST_KEY.getKey());
        list.addAll(threadPoolConfigEntities);
    }

    @Override
    public void reportThreadPoolConfigParameter(ThreadPoolConfigEntity threadPoolConfigEntity) {
        // 缓存桶的key
        String cacheKey = RegistryEnumVO.THREAD_POOL_CONFIG_PARAMETER_LIST_KEY.getKey() + "_" + threadPoolConfigEntity.getAppName() + "_" + threadPoolConfigEntity.getThreadPoolName();
        // 获取缓存桶
        RBucket<ThreadPoolConfigEntity> bucket = redisClient.getBucket(cacheKey);
        // 写入数据
        bucket.set(threadPoolConfigEntity, Duration.ofDays(30));

    }
}
