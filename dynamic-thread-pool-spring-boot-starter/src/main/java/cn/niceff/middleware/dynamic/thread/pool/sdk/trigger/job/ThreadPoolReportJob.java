package cn.niceff.middleware.dynamic.thread.pool.sdk.trigger.job;

import cn.niceff.middleware.dynamic.thread.pool.sdk.domain.IPoolService;
import cn.niceff.middleware.dynamic.thread.pool.sdk.domain.model.entity.ThreadPoolConfigEntity;
import cn.niceff.middleware.dynamic.thread.pool.sdk.registry.IRegistry;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.List;

/**
 * @author niceff.cn
 * @apiNote 线程池上报任务
 */
public class ThreadPoolReportJob {
    private final Logger logger = LoggerFactory.getLogger(ThreadPoolReportJob.class);

    /*
    一边拿数据, 一边上报
     */

    private final IPoolService poolService;

    private final IRegistry registry;

    public ThreadPoolReportJob(IPoolService poolService, IRegistry registry) {
        this.poolService = poolService;
        this.registry = registry;
    }

    @Scheduled(cron = "0/20 * * * * ?")
    public void executeReportThreadPoolList() {
        // 查询线程池的列表
        List<ThreadPoolConfigEntity> threadPoolConfigEntities = poolService.queryThreadPoolList();
        // 上报给redis
        registry.reportThreadPool(threadPoolConfigEntities);
        logger.info("上报线程池列表和单体信息: {}", JSON.toJSONString(threadPoolConfigEntities));
        for(ThreadPoolConfigEntity entity : threadPoolConfigEntities) {
            registry.reportThreadPoolConfigParameter(entity);
        }
    }
}
