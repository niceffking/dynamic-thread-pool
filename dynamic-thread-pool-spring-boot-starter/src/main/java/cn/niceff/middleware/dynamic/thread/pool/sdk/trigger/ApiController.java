package cn.niceff.middleware.dynamic.thread.pool.sdk.trigger;

import cn.niceff.middleware.dynamic.thread.pool.sdk.domain.model.entity.ThreadPoolConfigEntity;
import cn.niceff.middleware.dynamic.thread.pool.sdk.domain.model.valobj.RegistryEnumVO;
import cn.niceff.middleware.dynamic.thread.pool.sdk.types.Response;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RBucket;
import org.redisson.api.RList;
import org.redisson.api.RTopic;
import org.redisson.api.RedissonClient;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author niceff.cn
 * 对外提供接口
 */
@Slf4j
@RestController
@CrossOrigin("*")
@RequestMapping("api/thread-pool/")
public class ApiController {
    @Resource
    public RedissonClient redissonClient;


    /**
     * 查询线程池配置列表
     *
     * @return 线程池配置列表
     */
    @GetMapping("query-thread-pool-list")
    public Response<List<ThreadPoolConfigEntity>> queryThreadPoolList() {
        try {
            RList<ThreadPoolConfigEntity> list = redissonClient.getList(RegistryEnumVO.THREAD_POOL_CONFIG_LIST_KEY.getKey());
            List<ThreadPoolConfigEntity> threadPoolConfigEntities = list.readAll();
            return new Response<>(Response.CodeEnum.SUCCESS.getCode(), Response.CodeEnum.SUCCESS.getMessage(), threadPoolConfigEntities);
        } catch (Exception e) {
            log.error("查询线程池配置列表失败", e);
            return new Response<>(Response.CodeEnum.FAIL.getCode(), Response.CodeEnum.FAIL.getMessage(), null);
        }
    }

    /**
     * 查询单个一条的配置
     * @param appName 应用名称
     * @return 线程池配置列表
     */
    @GetMapping("query-thread-pool/query")
    public Response<ThreadPoolConfigEntity> queryThreadPoolListByAppName(@RequestParam("appName") String appName,  @RequestParam String threadPoolName) {
        try {
            String cacheKey = RegistryEnumVO.THREAD_POOL_CONFIG_PARAMETER_LIST_KEY.getKey() + "_" + appName + "_" + threadPoolName;
            RBucket<ThreadPoolConfigEntity> threadPoolConfigEntity = redissonClient.getBucket(cacheKey);
            ThreadPoolConfigEntity threadPoolEntity = threadPoolConfigEntity.get();
            return new Response<>(Response.CodeEnum.SUCCESS.getCode(), Response.CodeEnum.SUCCESS.getMessage(), threadPoolEntity);
        } catch (Exception e) {
            log.error("查询线程池配置列表失败", e);
            return new Response<>(Response.CodeEnum.FAIL.getCode(), Response.CodeEnum.FAIL.getMessage(), null);
        }
    }


    @RequestMapping(value = "update_thread_pool_config", method = RequestMethod.POST)
    public Response<Boolean> updateThreadPoolConfig(@RequestBody ThreadPoolConfigEntity request) {
        try {
            log.info("修改线程池配置开始 {} {} {}", request.getAppName(), request.getThreadPoolName(), JSON.toJSONString(request));
            RTopic topic = redissonClient.getTopic("DYNAMIC_THREAD_POOL_REDIS_TOPIC" + "_" + request.getAppName());
            topic.publish(request);
            log.info("修改线程池配置完成 {} {}", request.getAppName(), request.getThreadPoolName());
            return Response.<Boolean>builder()
                    .code(Response.CodeEnum.SUCCESS.getCode())
                    .message(Response.CodeEnum.SUCCESS.getMessage())
                    .data(true)
                    .build();
        } catch (Exception e) {
            log.error("修改线程池配置异常 {}", JSON.toJSONString(request), e);
            return Response.<Boolean>builder()
                    .code(Response.CodeEnum.FAIL.getCode())
                    .message(Response.CodeEnum.FAIL.getMessage())
                    .data(false)
                    .build();
        }
    }

}
