package cn.niceff.middleware.dynamic.thread.pool.sdk.trigger.listener;

import cn.niceff.middleware.dynamic.thread.pool.sdk.domain.IPoolService;
import cn.niceff.middleware.dynamic.thread.pool.sdk.domain.model.entity.ThreadPoolConfigEntity;
import cn.niceff.middleware.dynamic.thread.pool.sdk.registry.IRegistry;
import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.redisson.api.RTopic;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import java.util.List;
import java.util.concurrent.CountDownLatch;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ThreadPoolConfigListenerTest {
    @Resource
    private RTopic dynamicThreadPoolRedisTopic;

    @Resource
    private IPoolService poolService;

    @Resource
    private IRegistry registry;

    @Test
    public void test_dynamicThreadPoolRedisTopic() throws InterruptedException {
        ThreadPoolConfigEntity threadPoolConfigEntity = new ThreadPoolConfigEntity("dynamic-thread-pool", "threadPoolExecutor01");
        threadPoolConfigEntity.setMaximumPoolSize(100);
        threadPoolConfigEntity.setCorePoolSize(50);
        dynamicThreadPoolRedisTopic.publish(threadPoolConfigEntity);

        Thread.sleep(1000);
        List<ThreadPoolConfigEntity> threadPoolConfigEntities = poolService.queryThreadPoolList();
        threadPoolConfigEntities.stream().forEach(x -> {
            if (x.getThreadPoolName().equals("threadPoolExecutor01")) {
                assertEquals(100, x.getMaximumPoolSize());
                assertEquals(50, x.getCorePoolSize());
            }
        });
                // 查看内存中是否已经被修改
        ThreadPoolConfigEntity threadPoolConfigEntity1 = poolService.queryThreadPoolConfigByName("threadPoolExecutor01");
        assertEquals(100, threadPoolConfigEntity1.getMaximumPoolSize());
        assertEquals(50, threadPoolConfigEntity1.getCorePoolSize());
    }
}