package cn.niceff.middleware.dynamic.thread.pool.sdk.domain;

import cn.niceff.middleware.dynamic.thread.pool.sdk.domain.model.entity.ThreadPoolConfigEntity;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@RunWith(SpringRunner.class)
public class PoolServiceImplTest {
    @Resource
    private IPoolService poolService;

    @Test
    public void queryThreadPoolList() {
        List<ThreadPoolConfigEntity> threadPoolConfigEntities = poolService.queryThreadPoolList();
        System.out.println(JSON.toJSONString(threadPoolConfigEntities));
    }

    @Test
    public void queryThreadPoolConfigByName() {
        ThreadPoolConfigEntity threadPoolConfigEntity = poolService.queryThreadPoolConfigByName("test");
        assert threadPoolConfigEntity == null;
        ThreadPoolConfigEntity threadPoolConfigEntity1 = poolService.queryThreadPoolConfigByName("threadPoolExecutor01");
        assert threadPoolConfigEntity1 != null;
        System.out.println(JSON.toJSONString(threadPoolConfigEntity1));
        ThreadPoolConfigEntity threadPoolConfigEntity2 = poolService.queryThreadPoolConfigByName("threadPoolExecutor02");
        assert threadPoolConfigEntity2 != null;
        System.out.println(JSON.toJSONString(threadPoolConfigEntity2));
    }

    @Test
    public void updateThreadPoolConfig() {
        ThreadPoolConfigEntity entity = new ThreadPoolConfigEntity();
        entity.setAppName("dynamic-thread-pool");
        entity.setThreadPoolName("threadPoolExecutor01");
        entity.setCorePoolSize(50);
        entity.setMaximumPoolSize(100);
        poolService.updateThreadPoolConfig(entity);
        ThreadPoolConfigEntity threadPoolConfigEntity = poolService.queryThreadPoolConfigByName("threadPoolExecutor01");
        assertEquals(100, threadPoolConfigEntity.getMaximumPoolSize());
        assertEquals(50, threadPoolConfigEntity.getCorePoolSize());

    }
}